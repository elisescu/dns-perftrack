/*
 * DNSWorker.h
 *
 *  Created on: Mar 8, 2014
 *      Author: elisescu
 */

#ifndef DNSWORKER_H_
#define DNSWORKER_H_

#include <mysql++.h>
#include <thread>
#include <unistd.h>
#include <cstdlib>
#include <ssqls.h>

#include "utils.h"

using namespace std;
using namespace mysqlpp;

namespace dnsprf {

class DNSWorker {
public:
    virtual ~DNSWorker();
    DNSWorker(Connection& conn, site_t& dom);
    void run();
    void join();
    string* get_domain() {return domain;}
    bool running() {return still_running;}
private:
    Connection* connection;
    string* domain;
    int rank;
    thread* worker_thread;
    bool still_running;
    unsigned long long first_query_ts;
    unsigned long long last_query_ts;
    float query_taverage;
    float query_tsdev;
    unsigned long number_queries;
    float squared_sum;
    void _run();
    void dns_query();
};

} /* namespace dnsprf */

#endif /* DNSWORKER_H_ */
