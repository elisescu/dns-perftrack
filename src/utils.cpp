/*
 * utils.cpp
 *
 *  Created on: Mar 6, 2014
 *      Author: elisescu
 */

#include "utils.h"

#define DEBUG_EN 1
#include "log.h"

using namespace std;

namespace dnsprf {
const string Utils::DATABASE_NAME = "dns_performance_db";
const string Utils::TABLE_SITE_AVG_NAME = "domain_data";
const string Utils::SITE_RANK_KEY = "site_rank";
const string Utils::SITE_NAME_KEY = "site_name";
const string Utils::SITE_AVG_TIME_KEY = "site_avg_time";
const string Utils::SITE_STDDEV_TIME_KEY = "site_stdv_time";
const string Utils::SITE_QUERYNO_KEY =  "site_no_queries";
const string Utils::SITE_QUERYSQSUM_KEY = "site_sqsum_time";
const string Utils::SITE_FIRST_QUERY_KEY = "site_first_query";
const string Utils::SITE_LAST_QUERY_KEY = "site_last_query";
const string Utils::CREATE_AVG_TABLE = "CREATE TABLE " + Utils::TABLE_SITE_AVG_NAME + "( "
        + Utils::SITE_RANK_KEY + " int primary key, "
        + Utils::SITE_NAME_KEY + " varchar(255) not null, "
        + Utils::SITE_LAST_QUERY_KEY + " bigint, "
        + Utils::SITE_FIRST_QUERY_KEY + " bigint, "
        + Utils::SITE_AVG_TIME_KEY + " float, "
        + Utils::SITE_STDDEV_TIME_KEY+ " float, "
        + Utils::SITE_QUERYNO_KEY + " bigint ,"
        + Utils::SITE_QUERYSQSUM_KEY + " float "
        +");";

mutex Utils::GLOBAL_DB_MUTEX;

map<unsigned int, unsigned long long> Utils::time_measurements;
unsigned int Utils::unique_id = 0;
mutex Utils::time_mutex;

bool Utils::table_exists(Connection conn, string table) {
    LOGD("Checking whether table <%s> exists.", table.c_str());
    if (!conn.connected()) {
        LOGD("We are not connected to the server");
        return false;
    }

    try {
        // list all the tables from our DB and see if the table exists.
        Query query = conn.query("show tables from " + Utils::DATABASE_NAME);
        StoreQueryResult res = query.store();
        if (res != NULL && res.size() > 0) {
            StoreQueryResult::const_iterator it;
            for (it = res.begin(); it != res.end(); ++it) {
                mysqlpp::Row row = *it;
                if (row[0] == table) {
                    LOGD("Table %s exists !!", row[0].c_str());
                    return true;
                }
            }
        }
    } catch (BadQuery& e) {
        LOGD("Table doesn't exist");
    }
    return false;
}

void Utils::table_create(Connection conn, string tcs) {
    /* TODO: maybe change the name of the method to a proper one.
     * this mehtod just executes a query so maybe find a better way for this.
     */
    LOGD("Executing query <%s> .", tcs.c_str());
    if (!conn.connected()) {
        LOGD("We are not connected to the server");
        return;
    }
    Query query = conn.query(tcs);
    query.execute();
}

void Utils::setup_DB(Connection conn) {
    // create the DB if it doesn't exist
    try {
        conn.select_db(Utils::DATABASE_NAME);
    } catch (Exception& e) {
        conn.create_db(Utils::DATABASE_NAME);
        conn.select_db(Utils::DATABASE_NAME);
    }

    if (!Utils::table_exists(conn, Utils::TABLE_SITE_AVG_NAME)) {
        Utils::table_create(conn, Utils::CREATE_AVG_TABLE);
    }
}

unsigned int Utils::start_time()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    unsigned long long millisSinceEpoch = (unsigned long long)(tv.tv_sec) * 1000 + (unsigned long long)(tv.tv_usec) / 1000;
    Utils::time_mutex.lock();
    unsigned int who = ++Utils::unique_id;
    Utils::time_measurements[who] = millisSinceEpoch;
    Utils::time_mutex.unlock();
    return who;
}

unsigned long int Utils::passed_time(int who)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    unsigned long long millisSinceEpoch = (unsigned long long)(tv.tv_sec) * 1000 + (unsigned long long)(tv.tv_usec) / 1000;
    Utils::time_mutex.lock();
    unsigned long long time_then = Utils::time_measurements[who];
    Utils::time_measurements.erase(who);
    Utils::time_mutex.unlock();
    return (unsigned long)(millisSinceEpoch - time_then);
}

} /* namespace dnsprf */
