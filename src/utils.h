/*
 * utils.h
 *
 *  Utility class store constants related to the database tables and also do
 *  some useful.
 *
 *  Created on: Mar 6, 2014
 *      Author: elisescu
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <string>
#include <mysql++.h>
#include <map>
#include <sys/time.h>
#include <mutex>
#include <string>



namespace dnsprf {

using namespace std;
using namespace mysqlpp;

typedef struct{
    int rank;
    string domain;
} site_t;


class Utils {
public:
    /**
     * Database name
     */
    static const string DATABASE_NAME;
    /**
     * The name of the first table, where we store average values
     */
    static const string TABLE_SITE_AVG_NAME;
    /**
     * The site ID key name
     */
    static const string SITE_ID_KEY;
    /**
     * The site rank key name
     */
    static const string SITE_RANK_KEY;
    /**
     * The key name of the site name value
     */
    static const string SITE_NAME_KEY;
    /**
     * The key name of the site average query time
     */
    static const string SITE_AVG_TIME_KEY;
    /**
     * The key name for the standard deviation of the queries time
     */
    static const string SITE_STDDEV_TIME_KEY;
    /**
     * The key name for the number of queries made so far
     */
    static const string SITE_QUERYNO_KEY;
    /**
     * The sum of the squares of the values. Used to compute
     * incrementally the standard deviation.
     */
    static const string SITE_QUERYSQSUM_KEY;
    /**
     * The key name for the first query made on the site
     */
    static const string SITE_FIRST_QUERY_KEY;
    /**
     * The key name for the last query made on the site
     */
    static const string SITE_LAST_QUERY_KEY;

    /*
     * The query to be executed in order to create the table, if it doesn't exists
     */
    static const string CREATE_AVG_TABLE;

    /*
     * Global mutex used by the threads to update the DB. Seems there are problems when using
     * multi-threading access to the DB... TODO: investigate more this problem.
     */
    static mutex GLOBAL_DB_MUTEX;

    static bool table_exists(Connection conn, string table_name);

    static void table_create(Connection conn, string table_creation_string);

    static void setup_DB(Connection conn);

    /*
     * Method used to start time tracking for somebody. Elapsed time will be retrieved
     * by passed_time() with the same int value as this method was called with.
     */
    static unsigned int start_time();

    /*
     * Get the passed time value from some entity with ID "who"
     */
    static unsigned long passed_time(int who);

private:
    //TODO: maybe use uint64_t
    static map<unsigned int, unsigned long long> time_measurements;
    static unsigned int unique_id;
    static mutex time_mutex;
};

} /* namespace dnsprf */

#endif /* UTILS_H_ */
