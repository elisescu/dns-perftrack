/*
 * WorkersManager.h
 *
 *  Created on: Mar 8, 2014
 *      Author: elisescu
 */

#ifndef WORKERSMANAGER_H_
#define WORKERSMANAGER_H_

#include <mysql++.h>
#include "DNSWorker.h"

namespace dnsprf {

class WorkersManager {
public:
    WorkersManager(Connection& conn, vector<site_t>& doms);
    virtual ~WorkersManager();
    void init();
    void run();
    void join();
    bool running();
private:
    Connection* connection;
    vector<DNSWorker*> dns_workers;
};

} /* namespace dnsprf */

#endif /* WORKERSMANAGER_H_ */
