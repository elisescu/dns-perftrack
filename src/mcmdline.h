/*
 * mcmdline.h
 *
 *  Utility class to parse the command line arguments. Based on mysql++ cmdline.
 *
 *  Created on: Mar 6, 2014
 *      Author: elisescu
 */

#ifndef MCMDLINE_H_
#define MCMDLINE_H_

#include <common.h>
#include <string>
#include <vector>
#include <assert.h>
#include <cmdline.h>

namespace dnsprf {
extern const char* db_name;
extern const char* table_name;

class CommandLine : public mysqlpp::CommandLineBase {
public:
    CommandLine(int argc, char* const argv[]);
    void print_usage() const;
    const int query_interval() const {return query_interval_;}
    const char* pass() const { return pass_; }
    const char* server() const { return server_; }
    const char* user() const { return user_; }

private:
    int query_interval_;
    const char* server_;
    const char* user_;
    const char* pass_;
};
}



#endif /* MCMDLINE_H_ */
