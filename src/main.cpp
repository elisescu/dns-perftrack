/*
 * main.cpp
 *
 *  Created on: Mar 6, 2014
 *      Author: elisescu
 */

#include <iostream>
#include <mysql++.h>

#include "WorkersManager.h"
#include "DNSWorker.h"
#define DEBUG_EN 1
#define LOGTAG "main"
#include "log.h"
#include "mcmdline.h"
#include "utils.h"

using namespace std;
using namespace mysqlpp;
using namespace dnsprf;


int main(int argc, char *argv[]) {
    CommandLine cmdline(argc, argv);
    if (!cmdline) {
        return 1;
    }

    Connection con(true);
    vector<site_t> domains(0);
    /*
     * TODO: read these from a file maybe or from a DB?.
     * For now just have them here..not important for the task
     */
    domains.push_back({1, "google.com"});
    domains.push_back({2, "facebook.com"});
    domains.push_back({3, "youtube.com"});
    domains.push_back({4, "yahoo.com"});
    domains.push_back({5, "live.com"});
    domains.push_back({6, "wikipedia.org"});
    domains.push_back({7, "baidu.com"});
    domains.push_back({8, "blogger.com"});
    domains.push_back({9, "msn.com"});
    domains.push_back({10, "qq.com"});

    try {
        con.connect(0, cmdline.server(),
                cmdline.user(), cmdline.pass());

        // create the DB if it doesn't exist
        try {
            con.select_db(Utils::DATABASE_NAME);
        } catch (Exception& e) {
            con.create_db(Utils::DATABASE_NAME);
            con.select_db(Utils::DATABASE_NAME);
        }

        if (!Utils::table_exists(con, Utils::TABLE_SITE_AVG_NAME)) {
            Utils::table_create(con, Utils::CREATE_AVG_TABLE);
        }
    } catch (mysqlpp::Exception& e) {
        LOGE("Something went wrong: %s", e.what());
        return -1;
    }

    WorkersManager wm(con, domains);
    LOGD("Starting the DNS queries with %d ms period!!", cmdline.query_interval());
    for (;;)
    {
        unsigned int t_id = Utils::start_time();
        wm.run();
        wm.join();
        unsigned long ptime = Utils::passed_time(t_id);
        unsigned long sleep_time = 0;
        if ((unsigned int)cmdline.query_interval() > ptime) {
            sleep_time = (unsigned int)cmdline.query_interval() - ptime;
        }
        LOGD("Queries took %ld ms. Remaining to sleep %ld ms... ", ptime, sleep_time );
        usleep(sleep_time * 1000);
    }
	return 0;
}

