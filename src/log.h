/*
 * log.h
 *
 *  Utility file used logging.
 *
 *  Created on: Mar 6, 2014
 *      Author: elisescu
 */

#ifndef LOG_H_
#define LOG_H_

#include <stdio.h>
/*
 * For now have only two levels of debugging: error and debug.
 */
#ifndef LOGTAG
#define LOGTAG "--"
#endif

#if DEBUG_EN
#define LOGD(args...) printf("\nD(" LOGTAG "):" args);
#define LOGI(args...) printf("\nI(" LOGTAG "):" args);
#else
#define LOGD(args...)
#define LOGI(args...)
#endif

#define LOGE(args...) fprintf(stderr, "\nE(" LOGTAG "):" args);

#endif /* LOG_H_ */
