/*
 * DNSWorker.cpp
 *
 *  Created on: Mar 8, 2014
 *      Author: elisescu
 */


#include <math.h>
#include <ldns/ldns.h>

#include "DNSWorker.h"
#include "utils.h"

#define DEBUG_EN 0
#define LOGTAG "DNSWorker"
#include "log.h"

//IMPORTANT !! these names have to be exactly the same as the ones declared in UTILS..
// maybe should have used #defines instead of consts ! :(

//        + Utils::SITE_RANK_KEY + " int primary key, "
//        + Utils::SITE_NAME_KEY + " varchar(255) not null, "
//        + Utils::SITE_LAST_QUERY_KEY + " bigint, "
//        + Utils::SITE_FIRST_QUERY_KEY + " bigint, "
//        + Utils::SITE_AVG_TIME_KEY + " float, "
//        + Utils::SITE_STDDEV_TIME_KEY+ " float, "
//        + Utils::SITE_QUERYNO_KEY + " bigint ,"
//        + Utils::SITE_QUERYSQSUM_KEY + " float "

sql_create_8(domain_data,
    2, 8,
    sql_int, site_rank,
    sql_text, site_name,
    sql_bigint, site_last_query,
    sql_bigint, site_first_query,
    sql_double, site_avg_time,
    sql_double, site_stdv_time,
    sql_bigint, site_no_queries,
    sql_double, site_sqsum_time
    )

namespace dnsprf {

DNSWorker::DNSWorker(Connection&  conn, site_t& dom):
            connection(&conn), domain(&dom.domain), rank(dom.rank), worker_thread(0), still_running(false),
            first_query_ts(0), last_query_ts(0), query_taverage(0), query_tsdev(0),
            number_queries(0), squared_sum(0)
{
    /*TODO: query the DB for the current number of queries, current average, etc...
     * If they don't exist, then create the row in the table so that later on we
     * can just update it.
     */
    /*
     * TODO: maybe do this in an bool init() method and not in the constructor...?
     */
    stringstream squery_val;
    bool have_init_data = false;

    squery_val << "select * from " << Utils::TABLE_SITE_AVG_NAME + " where "
            + Utils::SITE_NAME_KEY +"=\'" << domain->c_str() << "\';";

    Utils::GLOBAL_DB_MUTEX.lock();
    Query query = connection->query(squery_val.str());
    Utils::GLOBAL_DB_MUTEX.unlock();
    vector<domain_data> res;
    query.storein(res);
    if (res.size() > 0 && res[0].site_rank == rank) {
        have_init_data = true;
        LOGD("Found initial data for %s in the DB. Using those:", domain->c_str());
        domain_data irow = res[0];
        last_query_ts = (unsigned long long) irow.site_last_query;
        first_query_ts = (unsigned long long) irow.site_first_query;
        query_taverage = irow.site_avg_time;
        query_tsdev = irow.site_stdv_time;
        number_queries = irow.site_no_queries;
        squared_sum = irow.site_sqsum_time;
        LOGD("   -> (%lld, %lld, %f, %f, %ld, %f), ", last_query_ts, first_query_ts,
                query_taverage, query_tsdev, number_queries, squared_sum);
    }

    if (!have_init_data) {
        Utils::GLOBAL_DB_MUTEX.lock();
        LOGD("Didn't have any row for %s.. so creating one...", domain->c_str());
        Query query = connection->query();
        domain_data irow(rank, domain->c_str(), 0, 0, 0, 0, 0, 0);
        query.insert(irow);
        LOGD("Query executed: %s", query.str().c_str());
        query.execute();
        Utils::GLOBAL_DB_MUTEX.unlock();
    }
}

DNSWorker::~DNSWorker()
{
    LOGD("Destroying worker for %s", domain->c_str());
}

void DNSWorker::_run()
{
    // get the time stamp of the first query and add it to the DB
    if (first_query_ts == 0) {
        struct timeval tv;
        gettimeofday(&tv, NULL);
        first_query_ts = (unsigned long long)(tv.tv_sec) * 1000 + (unsigned long long)(tv.tv_usec) / 1000;
    }

    struct timeval tv;
    gettimeofday(&tv, NULL);
    last_query_ts = (unsigned long long)(tv.tv_sec) * 1000 + (unsigned long long)(tv.tv_usec) / 1000;

    // do the DNS request and get the latency
    int my_tmid = Utils::start_time();

// when defining DEBUG_CALCS_AND_DB then the DB looks pretty nice with consistent ~0.279 deviation
#ifdef DEBUG_CALCS_AND_DB
    usleep(40000);
#else
    dns_query();
#endif

    unsigned long dtime = Utils::passed_time(my_tmid);

    // compute the average based on the accumulated average and the current latency value
    query_taverage = ((query_taverage * (float) number_queries) + (float) dtime) / (number_queries + 1);
    number_queries++;

    // now compute the incremental standard deviation: stdev = sqrt((sum_x2 / n) - (mean * mean))
    squared_sum += (float) dtime * (float) dtime;
    query_tsdev = sqrtf((squared_sum / number_queries) - (query_taverage * query_taverage));

    // now update these values in the database
    try {
        LOGD("Update data for %s...", domain->c_str());
        domain_data irow(rank, domain->c_str(), last_query_ts, first_query_ts,
                query_taverage, query_tsdev, number_queries, squared_sum);
        Utils::GLOBAL_DB_MUTEX.lock();
        Query query = connection->query();
        query.replace(irow);
        query.execute();
        Utils::GLOBAL_DB_MUTEX.unlock();
        LOGD("Query executed: %s", query.str().c_str());
    } catch (mysqlpp::Exception& e) {
        Utils::GLOBAL_DB_MUTEX.unlock();
        //TODO: handle this so that maybe we stop the main loop.
        LOGE("Something went wrong when trying to write to DB: %s ", e.what());
    }

    // TODO: is this really the way to check whether the thread is still running? find another way..
    still_running = false;
}

void DNSWorker::dns_query()
{
    ldns_resolver *res = NULL;
    ldns_rdf *domain_rdf = NULL;
    ldns_status s;
    ldns_pkt *p = NULL;
    ldns_rr_type query_type = LDNS_RR_TYPE_A;
    stringstream rand_subdomain;

    //TODO: find a nicer way to get this random subdomain
    rand_subdomain << "subdom" << number_queries << "." << domain->c_str();

    domain_rdf = ldns_dname_new_frm_str(rand_subdomain.str().c_str());
    if (!domain_rdf) {
        LOGE("Cannot create DNS rdf structure for domain %s", rand_subdomain.str().c_str());
        return;
    }

    s = ldns_resolver_new_frm_file(&res, NULL);
    if (s != LDNS_STATUS_OK) {
        LOGE("Cannot configure a resolver for domain %s", rand_subdomain.str().c_str());
        return;
    }

    p = ldns_resolver_query(res,
            domain_rdf,
            query_type,
            LDNS_RR_CLASS_IN,
            LDNS_RD);

//#define SHOW_QUERY_REPLY
#ifdef SHOW_QUERY_REPLY
    ldns_rr_list *rpack = NULL;
    rpack = ldns_pkt_rr_list_by_type(p, query_type, LDNS_SECTION_ANSWER);

    if (rpack) {
        ldns_rr_list_print(stdout, rpack);
        ldns_rr_list_deep_free(rpack);
    } else {
        LOGE("Cannot list the result from the query");
    }
#endif

    ldns_rdf_deep_free(domain_rdf);
    ldns_resolver_deep_free(res);
    ldns_pkt_free(p);
}

void DNSWorker::run()
{
    LOGD("Starting the working thread for %s ", domain->c_str());
    still_running = true;
    worker_thread = new thread(&DNSWorker::_run, this);
}

void DNSWorker::join() {
    LOGD("Waiting to finish DNS worker for %s ", domain->c_str());
    if (worker_thread->joinable()) {
        worker_thread->join();
        delete worker_thread;
    }
    LOGD("DNS worker for %s finished", domain->c_str());
}

} /* namespace dnsprf */
