/*
 * WorkersManager.cpp
 *
 *  Created on: Mar 8, 2014
 *      Author: elisescu
 */

#include "WorkersManager.h"

#define DEBUG_EN 0
#define LOGTAG "WorkersManager"
#include "log.h"

namespace dnsprf {

WorkersManager::WorkersManager(Connection& conn, vector<site_t>& doms):
                connection(&conn), dns_workers(0)
{
    LOGD("Creating workers manager for %d workers", (int)doms.size());
    for (uint32_t i = 0; i < doms.size(); i++)
    {
        DNSWorker* dw = new DNSWorker(conn, doms[i]);
        dns_workers.push_back(dw);
    }
}

WorkersManager::~WorkersManager()
{
    LOGD("Destroying the workers manager and its workers");
    for (uint32_t i = 0; i < dns_workers.size(); i++)
    {
        delete dns_workers[i];
    }
}

void WorkersManager::init()
{

}

void WorkersManager::join()
{
    LOGD("Joining the workers...")
    for (uint32_t i = 0; i < dns_workers.size(); i++)
    {
        DNSWorker *dw = dns_workers[i];
        dw->join();
    }
}

void WorkersManager::run()
{
    for (uint32_t i = 0; i < dns_workers.size(); i++)
    {
        DNSWorker *dw = dns_workers[i];
        dw->run();
    }
}

bool WorkersManager::running()
{
    for (uint32_t i = 0; i < dns_workers.size(); i++)
    {
        DNSWorker *dw = dns_workers[i];
        if (dw->running()) {
            return true;
        }
    }
    return false;
}

} /* namespace dnsprf */
