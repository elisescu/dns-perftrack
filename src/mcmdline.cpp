/*
 * mcmdline.cpp
 *
 * Utility class to parse the command line based on the sql++ comdline.
 *
 *  Created on: Mar 6, 2014
 *      Author: elisescu
 */

#include "mcmdline.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

namespace dnsprf
{
CommandLine::CommandLine(int argc, char* const argv[]) : CommandLineBase(argc, argv, "i:p:s:u:"),
        query_interval_(-1),
        server_(""),
        user_(""),
        pass_("")
{
    int ch;
    if (argc < 4) {
        parse_error();
        return;
    }

    while (successful() && ((ch = parse_next()) != EOF)) {
        switch (ch) {
        case 'i': query_interval_ = atoi(option_argument()); break;
        case 'p': pass_ = option_argument();           break;
        case 's': server_ = option_argument();         break;
        case 'u': user_ = option_argument();           break;
        default:
            parse_error();
            return;
        }
    }
    finish_parse();
}

void CommandLine::print_usage() const
{
    std::cout << "usage: " << program_name() <<
            " -i <query interval time in ms> -s <server_addr> -u <user> -p <password> " << std::endl;
    std::cout << std::endl;
}
}


