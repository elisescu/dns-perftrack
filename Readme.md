Tracking DNS performance to top sites
=================================
C++ Coding Challenge

Importance: Such performance numbers to top sites can be used as benchmarks for others to compare to.

Problem description:
--------------

Write a c++ program on Linux/BSD(Mac) that periodically sends DNS queries to the nameservers of the top 10 Alexa domains and stores the latency values in a mysql table. The frequency of queries should be specified by the user on command line. The program needs to make sure it doesn't hit the DNS cache while trying to query for a site and should use a random string prepended to a domain. E.g. to query foo.bar, make sure to prepend a random string, e.g. 1234xpto.foo.bar.

Besides the timeseries values, the code needs to keep track in db stats per domain about:

- the average query times

- standard deviation of DNS query times

- number of queries made so far

- time stamp of first query made per domain and last query made


Solution description
--------------
```
./dns-perftrack 
usage: ./dns-perftrack -i <query interval time in ms> -s <server_addr> -u <user> -p <password>
```
The program takes as arguments the value of the period of issuing the DNS
queries (in ms), the server address and the credentials of the user that
will create the DB. Make sure the user has permissions to create a DB and
tables.

The program will check whether the database and the table exist and creates
them if they don't exist. Afterwards, it will periodically issue DNS queries
to different (non-existing) subdomains of the Alexa top 10 websites measuring
the latency of the DNS query responses. The registered timestamps (for the
first query and for the last one) are stored as milliseconds since the epoch.
The 10 websites are statically initialized in main.cpp (soon maybe they will
be read from a configuration file or maybe a DB). The DB-related names (table,
database, rows, etc) are all present in utils.cpp file (database name="dns_performance_db",
table name="domain_data"). For now, the program starts 10 threads (one for each
of the 10 domains), reads the existing data about that domain from the DB
(if it exists), and continues from there for each of the domains with the queries,
issuing them in parallel. However, when it is about reading/storing the 
data from/to the DB there are some concurency problems, so I had to use a mutex
for now, to serialize the access.    


Installation
--------------

1 Compile and install mysql++:
```
  ./configure --with-mysql-lib=/usr/lib/x86_64-linux-gnu/ --prefix=${HOME}/opt/
  make -j 6
  make install
```

2. Compile and install ldns:
```
  ./configure --prefix=${HOME}/opt/
  make -j 6
  make install
```

3. Compile and run the challenge software
```
 cd ${SOME_WHERE}
 git clone https://bitbucket.org/elisescu/dns-perftrack.git
 cd dns-perftrack
 make
 export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${HOME}/opt/lib
 ./dns-perftrack -i 3000 -s localhost -u dnsuser -p dnspass
```
Note here that the mysql++ and ldns libraries were installed in ${HOME}/opt/, but any folder can be used. If you use a different folder, make sure you update the Makefile (INCLUDE_DIR_FLAGS and LIB_DIR_FLAGS variables to include the folder you're using) before compiling and the LD_LIBRARY_PATH env variable before running. Adding autoconf support soon to avoid all this mess.

If you get "error while loading shared libraries" then do a "locate" on the missing library and add the folders containing those libraries to the LD_LIBRARY_PATH (e.g.: export LD_LIBRARY_PATH=/usr/local/lib/) before running again.


Sample data stored in the DB after 202 queries:
-----
![dns-perftracker.png](https://bitbucket.org/repo/pgGE9a/images/4248254238-dns-perftracker.png)

Valgrind report regarding memory leaks after aprox. 400 iterations:
----
```
==25380== LEAK SUMMARY:
==25380==    definitely lost: 0 bytes in 0 blocks
==25380==    indirectly lost: 0 bytes in 0 blocks
```

Known issues:
--------

Sometimes the program crashes somewhere in the SSL library (called via ldns_resolver_query) with a seg fault.. It tries to generate a random number but for some reason it crashes..
Stacktrace here:

```
#0  0x00007ffff627d40b in ?? () from /lib/x86_64-linux-gnu/libcrypto.so.1.0.0
#1  0xa48bc82cb3a3a6c9 in ?? ()
#2  0xc39292f992a2f606 in ?? ()
#3  0xac62f79cfc72cee2 in ?? ()
#4  0xa333709161766099 in ?? ()
#5  0xe3ea6cd1ac8491e9 in ?? ()
#6  0xbfa7fc959a949ef5 in ?? ()
#7  0x150c300d219211c4 in ?? ()
#8  0xb4c856a7581cfcaa in ?? ()
#9  0x00007ffff65e893c in ?? () from /lib/x86_64-linux-gnu/libcrypto.so.1.0.0
#10 0x00007ffff0000d60 in ?? ()
#11 0x0000000000000003 in ?? ()
#12 0x00007ffff6279820 in SHA1_Update () from /lib/x86_64-linux-gnu/libcrypto.so.1.0.0
#13 0x00007ffff62fbafb in ?? () from /lib/x86_64-linux-gnu/libcrypto.so.1.0.0
#14 0x00007ffff62fbeb5 in ?? () from /lib/x86_64-linux-gnu/libcrypto.so.1.0.0
#15 0x00007ffff7bc2e3a in ldns_get_random () at ./util.c:404
#16 0x00007ffff7bb0799 in ldns_pkt_set_random_id (packet=0x7ffff0000bf0) at ./packet.c:458
#17 0x00007ffff7bb72d8 in ldns_resolver_prepare_query_pkt (query_pkt=query_pkt@entry=0x7ffff5df48d0, r=r@entry=0x7ffff0000e30, name=name@entry=0x7ffff0000bb0, t=t@entry=LDNS_RR_TYPE_A, 
    c=c@entry=LDNS_RR_CLASS_IN, flags=flags@entry=8) at ./resolver.c:1199
#18 0x00007ffff7bb7463 in ldns_resolver_send (answer=0x7ffff5df4928, r=0x7ffff0000e30, name=0x7ffff0000bb0, t=LDNS_RR_TYPE_A, c=LDNS_RR_CLASS_IN, flags=<optimized out>) at ./resolver.c:1235
#19 0x00007ffff7bb7676 in ldns_resolver_query (r=<optimized out>, name=<optimized out>, t=<optimized out>, c=<optimized out>, flags=<optimized out>) at ./resolver.c:1061
#20 0x000000000040a44b in dnsprf::DNSWorker::dns_query() ()
#21 0x000000000040ade2 in dnsprf::DNSWorker::_run() ()
#22 0x00007ffff719f9f0 in ?? () from /usr/lib/x86_64-linux-gnu/libstdc++.so.6
#23 0x00007ffff65f3f8e in start_thread (arg=0x7ffff5df5700) at pthread_create.c:311
---Type <return> to continue, or q <return> to quit---
#24 0x00007ffff6903a0d in clone () at ../sysdeps/unix/sysv/linux/x86_64/clone.S:113
```
