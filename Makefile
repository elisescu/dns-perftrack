srcdir = .

CC = gcc
CCOMMON_FLAGS = -g -O0 -Wall --std=c++11 -Werror
#INCLUDE_DIR_FLAGS = -I. -I${srcdir} -I./../dep/ldns-1.6.17/include/ -I./../dep/mysql++-3.2.1/lib
INCLUDE_DIR_FLAGS = -I${srcdir}  -I/usr/local/include/mysql++/ -I/usr/include/mysql++ -I$(HOME)/opt/include/mysql++/
CFLAGS += ${INCLUDE_DIR_FLAGS} ${CCOMMON_FLAGS} -DMYSQLPP_MYSQL_HEADERS_BURIED

LD = g++
LIB_DIR_FLAGS = -L/usr/local/lib/ -L/usr/lib/ -L$(HOME)/opt/lib/
LIBS_FLAGS = -lldns -lmysqlpp -lmysqlclient
LDFLAGS += ${LIB_DIR_FLAGS} ${LIBS_FLAGS}

MAIN_SRCS = src/main.cpp\
			src/mcmdline.cpp\
			src/utils.cpp\
			src/DNSWorker.cpp\
			src/WorkersManager.cpp

MAIN_DEPS = src/mcmdline.h\
			src/utils.h\
			src/log.h\
			src/DNSWorker.h\
			src/WorkersManager.h

MAIN_OBJS = $(MAIN_SRCS:.cpp=.o)
MAIN_NAME = dns-perftrack


$(MAIN_NAME): $(MAIN_OBJS)
	$(LD) -o $@ $^ $(LDFLAGS)    
	@echo "Built " $@

#XXX: this will compile all the objects files all the time when one of the deps file is changed..can be improved
%.o : %.cpp $(MAIN_DEPS)
	$(CC) -c $< -o $@ $(CFLAGS)

all: dns-perftrack
	@echo "Done all targets"

clean:
	@echo "Cleaning..."
	rm -f $(MAIN_OBJS) $(MAIN_NAME)
